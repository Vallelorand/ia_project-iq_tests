import json
from PIL import Image
from Display.addShape import addShape
from Display.showGrid import showGridForAll

def addFigure(figures, image, id):
  for figure in figures:
    shape=figure['type']
    position=figure['position']
    size=figure['size']
    orientation=figure['orientation']
    color=figure['color']
    fill=figure['fill']
    addShape(image,shape, (position["x"] + 50*(id%3),position["y"] + 50*int(id/3)), size, orientation, color, fill)


def addSolution(figures, image, id):
  for figure in figures:
    shape=figure['type']
    position=figure['position']
    size=figure['size']
    orientation=figure['orientation']
    color=figure['color']
    fill=figure['fill']
    addShape(image,shape, (position["x"] + 50*(id%4),position["y"] + 50*int(id/4)), size, orientation, color, fill)

def readJson2 (path):
  with open(path, 'r') as file:
    data = json.load(file)
  image1 = Image.new('RGB', (750, 750), (255, 255, 255))
  image2 = Image.new('RGB', (1000, 500), (255, 255, 255))
  showGridForAll(image1, False)
  showGridForAll(image2, False)
  id1,id2 = 0,0
  for imageDesc in data:
    if imageDesc=='solutions':
      for solution in data[imageDesc] :
        addSolution(solution['figures'], image2, id2)
        id2+=1
    elif imageDesc != 'solutionIndex' :
      addFigure(data[imageDesc]['figures'], image1, id1)
      id1+=1

  image1.save('Pictures/generatedTEST.png')
  image2.save('Pictures/generatedSolution.png')
