from PIL import ImageDraw

scale = 5

# Draws a 50x50 grid in the image
def showGrid(im):
    draw = ImageDraw.Draw(im)
    for i in range(50):
        draw.line([(0,i*scale), (im.width,i*scale)], fill="grey", width=(3 if i%10==0 else 1))
        draw.line([(i*scale,0), (i*scale,im.height)], fill="grey", width=(3 if i%10==0 else 1))
        
def showGridForAll(im, grid):
    draw = ImageDraw.Draw(im)
    if grid:
        for i in range(150):
            draw.line([(0,i*scale), (im.width,i*scale)], fill="grey", width=(3 if i%10==0 else 1))
            draw.line([(i*scale,0), (i*scale,im.height)], fill="grey", width=(3 if i%10==0 else 1))
    draw.line([(250,0),(250,750)], fill="black", width=(3))
    draw.line([(500,0),(500,750)], fill="black", width=(3))
    draw.line([(750,0),(750,750)], fill="black", width=(3))
    draw.line([(0,250),(1000,250)], fill="black", width=(3))
    draw.line([(0,500),(1000,500)], fill="black", width=(3))
    draw.line([(0,750),(1000,750)], fill="black", width=(3))