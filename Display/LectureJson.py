import json
from PIL import Image
from Display.addShape import addShape
from Display.showGrid import showGrid

def addFigure (figures, image):
  for figure in figures:
    shape=figure['type']
    position=figure['position']
    size=figure['size']
    orientation=figure['orientation']
    color=figure['color']
    fill=figure['fill']
    addShape(image,shape, (position["x"],position["y"]), size, orientation, color, fill)

def readJson (path):
  with open(path, 'r') as file:
    data = json.load(file)
  for imageDesc in data:
    if imageDesc=='solutions':
      ind = 0
      for sol in data[imageDesc]:
        image = Image.new('RGB', (250, 250), (255, 255, 255))
        showGrid(image)
        addFigure(sol['figures'], image)
        image.save('Pictures/Soluce'+ str(ind) +'.png')
        ind += 1
      return
    else :
      image = Image.new('RGB', (250, 250), (255, 255, 255))
      showGrid(image)
      addFigure(data[imageDesc]['figures'], image)
      image.save('Pictures/'+imageDesc+'.png')
