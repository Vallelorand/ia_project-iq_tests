from PIL import ImageDraw
from math import sin, cos, sqrt, pi

scale = 5
width = 4

# https://pillow.readthedocs.io/en/stable/reference/ImageDraw.html#PIL.ImageDraw.ImageDraw.line
def addShape(image, type, position, size, orientation, color, fill):
    draw = ImageDraw.Draw(image)
    if type == "Circle":
        # [(x0, y0), (x1, y1)] defines the boudingbox
        [center_x, center_y] = [i*scale for i in position]
        real_size = size * scale
        box = [
            (center_x - real_size/2, center_y - real_size/2),
            (center_x + real_size/2, center_y + real_size/2),
        ]
        draw.ellipse(box, outline=color, width=width, fill=(color if fill else None))
    if type == "Square":
        [x, y] = [i*scale for i in position]
        real_size = size * scale
        box = [
            (x, y),
            (x + real_size, y + real_size),
        ]
        draw.rectangle(box, outline=color, width=width, fill=(color if fill else None))
    if type == "Triangle":
        [x, y] = [i*scale for i in position]
        real_size = size * scale
        # (center_x, center_y, radius)
        boundingCircle = (x, y, real_size/2)
        draw.regular_polygon(boundingCircle, 3, outline=color,fill=(color if fill else None))
    if type == "Line":
        [x, y] = [i*scale for i in position]
        real_size = size * scale
        angle_rad = -2*pi*orientation/360
        coords = [
            (x - real_size/2 * cos(angle_rad), y - real_size/2 * sin(angle_rad) ),
            (x + real_size/2 * cos(angle_rad), y + real_size/2 * sin(angle_rad) )
        ]
        draw.line(coords,  width=width, fill=color)
