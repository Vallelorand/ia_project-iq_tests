from PIL import Image
import sys
from Display.showGrid import showGrid
from Display.addShape import addShape
from Display.LectureJson import readJson
from Display.LectureJsonAllImg import readJson2


def testExecution():
  im = Image.new('RGB', (250, 250), (255, 255, 255))
  showGrid(im)
  addShape(im, "Circle", (10, 10), 10, 0, "#FF00FF", False)
  addShape(im, "Square", (10, 10), 10, 0, "#FF00FF", False)
  addShape(im, "Triangle", (10, 10), 10, 0, "#FF00FF", False)
  addShape(im, "Circle", (10, 10), 3, 0, "#FF00FF", True)
  addShape(im, "Line", (25, 25), 10, 45, "#FF00FF", True)
  im.save("tmp.png")

if len(sys.argv)<2:
	print("× Veuillez spécifier un JSON d'entrée.")
	exit(2)
else :
  # testExecution()
  # readJson(sys.argv[1])
  readJson2(sys.argv[1])
  print("Génération des images terminée.")
