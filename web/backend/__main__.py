import os
from flask import Flask, send_from_directory, Response, request, jsonify
#from Generator.__main__ import testExecution2
from Generator.__main__ import testUnary,generateRandomRPM
from PIL import Image
from urllib.parse import unquote
from Display.LectureJson import addFigure 
from Resolver.__main__ import findIndexSolutionByAI
import json
import io


app = Flask(__name__, static_folder='../frontend/dist')

# Serve React App
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')

@app.route('/newRPM')
def newRPM():
    rpm = generateRandomRPM()
    rpm.generateRPM()
    return rpm.generateJson()

@app.route('/figure/<description>')
def figure(description):
    # Parse description
    description = unquote(description)
    jsonDescription = json.loads(description)
    # Generate figure
    image = Image.new('RGB', (250, 250), (255, 255, 255))
    addFigure(jsonDescription['figures'], image)
    img_byte_arr = io.BytesIO()
    image.save(img_byte_arr, format='PNG')
    img_byte_arr = img_byte_arr.getvalue()
    return Response(img_byte_arr, mimetype='image/png')

@app.route('/solve', methods = ['POST'])
def solve():
    # Parse rpm
    jsonRpm = request.get_json()
    print(jsonRpm)
    # Process solution
    sol, _ = findIndexSolutionByAI(jsonRpm, True)
    sol = '{"sol": ' + str(sol) + '}'
    return json.loads(sol)

if __name__ == '__main__':
    app.run(use_reloader=True, port=5000, threaded=True, host="0.0.0.0")
