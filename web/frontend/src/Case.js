import React, { Component } from 'react';

export default class Case extends Component {

	render() {
		const params = encodeURIComponent(JSON.stringify(this.props.case))
		const url = `/figure/${params}`
		return (
			<img src={url} className="figure"/>
		)
	}
}