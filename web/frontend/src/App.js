import React, { Component } from 'react';
import './app.scss';
import Game from './Game'

export default class App extends Component {
  render() {
    return (
      <div>
        <h3>Welcome aboard !</h3>
        <br/>
        <p>You will have 8 pictures and your goal is to find which one is missing <br/><br/>
        </p>
        <Game/>
      </div>
    );
  }

}
