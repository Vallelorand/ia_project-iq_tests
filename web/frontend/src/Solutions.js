import React, { Component } from 'react';
import Case from "./Case";
export default class Solutions extends Component {

	constructor(props) {
		super(props);
		this.state = {
			selectedId: null
		}
	}
	
	select(caseId) {
		if (!this.props.frozen) this.props.onSelectChange(caseId)
	}

	render() {
		const s = this.props.solution
		const sel = this.props.selectedId	
		console.log(sel)
		return (
			<div>
				<h3>Pick a solution</h3>
				<table className="solutions">
					<tbody>
						<tr>
							<td onClick={() => this.select(0)} className={sel == 0 ? 'sel' : 'ns'}>
								 <Case case={s[0]} /> 
							</td>
							<td onClick={() => this.select(1)} className={sel == 1 ? 'sel' : 'ns'}>
								 <Case case={s[1]} /> 
							</td>
							<td onClick={() => this.select(2)} className={sel == 2 ? 'sel' : 'ns'}>
								 <Case case={s[2]}/> 
							</td>
							<td onClick={() => this.select(3)} className={sel == 3 ? 'sel' : 'ns'}>
								 <Case case={s[3]} /> 
							</td>
						</tr>
						<tr>
							<td onClick={() => this.select(4)} className={sel == 4 ? 'sel' : 'ns'}>
								 <Case case={s[4]} /> 
							</td>
							<td onClick={() => this.select(5)} className={sel == 5 ? 'sel' : 'ns'}>
								 <Case case={s[5]}/> 
							</td>
							<td onClick={() => this.select(6)} className={sel == 6 ? 'sel' : 'ns'}>
								 <Case case={s[6]} /> 
							</td>
							<td onClick={() => this.select(7)} className={sel == 7 ? 'sel' : 'ns'}>
								 <Case case={s[7]}/> 
							</td>
						</tr>
					</tbody>
					
				</table>
			</div>
			
		)
	}
}