import React, { Component } from 'react';
import Case from "./Case";
export default class Figures extends Component {
	
	render() {
	
		return (
			<table className="figures">
				<tbody>
					<tr>
						<td>
							<Case case={this.props.rpm.upperLeft}/>
						</td>
						<td>
							<Case case={this.props.rpm.upperCenter}/>
						</td>
						<td>
							<Case case={this.props.rpm.upperRight}/>
						</td>
					</tr>
					<tr>
						<td>
							<Case case={this.props.rpm.middleLeft}/>
						</td>
						<td>
							<Case case={this.props.rpm.middleCenter}/>
						</td>
						<td>
							<Case case={this.props.rpm.middleRight}/>
						</td>
					</tr>
					<tr>
						<td>
							<Case case={this.props.rpm.bottomLeft}/>
						</td>
						<td>
							<Case case={this.props.rpm.bottomCenter}/>
						</td>
						<td>
							<span className="interrogation">?</span>
						</td>
					</tr>
				</tbody>
			</table>
		)
	}
}