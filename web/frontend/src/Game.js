import React, { Component } from 'react';
import Solutions from "./Solutions";
import Figures from "./Figures";
import Case from "./Case";
export default class Game extends Component {

	constructor(props) {
		super(props);
		this.state = {
			selectedId: null,
			validate: false,
			rpm: null,
			isFrozen: false,
			iaResponse: null
		}
	}
	
	handleSelectChange(newId) {
		this.setState({selectedId: newId})
	}

	validate() {
		this.setState({
			validate: true,
			isFrozen: true
		})
	}

	createNewGame() {
		fetch("/newRPM")
		  	.then(r => r.json())
		  	.then(json => this.setState({
				rpm: json,
				selectedId: null,
				validate: false,
				isFrozen: false,
				iaResponse: null
			}))
	}

	solveWithIA() {
		const rpmAsJson = JSON.stringify(this.state.rpm)
		fetch(`/solve`, {method: 'POST', body: rpmAsJson, headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		  }})
		.then(r => r.json())
		.then(json => this.setState({
			iaResponse: json.sol
	  	}))
	}

	render() {
		const rpm = this.state.rpm
		const frozen = this.state.isFrozen
		const iaResponse = this.state.iaResponse
		const selectedId = this.state.selectedId
		const showAnswer = this.state.validate && (selectedId!=null)
		const anwserisTrue = showAnswer && selectedId === rpm.solutionIndex
		console.log(showAnswer, anwserisTrue)
		return (
			<div>
				<span className="btn" onClick={() => this.createNewGame()}>🔥 Start a game</span>
				<br/><br/>
				{
					rpm && 
					<div>
						<Figures rpm={rpm} />
						<Solutions 
						solution={rpm.solutions} 
						solIndex={rpm.solutionIndex}
						selectedId={selectedId}
						frozen={frozen}
						onSelectChange={change => this.handleSelectChange(change)}
						/>
						{ showAnswer && 
							<h2>
								{ anwserisTrue ? 
									'✅ Well done !' 
									:   
									<div>
										❌ Oops ! The right answer was the {rpm.solutionIndex + 1}-th case.
										<br/>
										<div className="border">
											<Case case={rpm.solutions[rpm.solutionIndex]}/>
										</div>
									</div>
								}
							</h2>
						} 
						<span className="btn" onClick={() => this.validate()}>✔️ Validate</span>
						{
							iaResponse!=null &&
							<div>
								<p>The IA thinks this answer is the right one :</p>
								<div className="border">
									<Case case={rpm.solutions[iaResponse]}/>
								</div>
							</div>
						}
						<span className="btn" onClick={() => this.solveWithIA()}>Try to solve with IA</span>
					</div>
				}
			</div>
		)
	}
}