set -e
cd web/frontend
rm -rf dist
npm run build
cd ../..
python3 -m web.backend