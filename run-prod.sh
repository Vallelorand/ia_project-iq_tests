set -e
cd web/frontend
rm -rf dist
npm run build-prod
cd ../..
python3 -m web.backend