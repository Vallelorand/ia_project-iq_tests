import numpy as np
import json

dicoFig = {"Circle": 1, "Line": 2, "Triangle": 3, "Square" : 4, "Nulle": 5}

def readJson (path, data=None):
    matrice = np.array([object,object,object,object,object,object,object,object])
    solutions = np.array([object,object,object,object,object,object,object,object])
    counter = 0
    counter2 = 0
    if data is None:
        with open(path, 'r') as file:
            data = json.load(file)
    soluInd = data['solutionIndex']
    for imageDesc in data:
        if imageDesc!='solutions' and imageDesc!='solutionIndex':
            v = createCase(data[imageDesc]['figures'])
            matrice[counter] = np.asarray(v)
            counter +=1
        elif imageDesc=='solutions' and imageDesc!='solutionIndex' :
            for solution in data[imageDesc] :
                v = createCase(solution['figures'])
                solutions[counter2] = np.asarray(v)
                counter2 +=1
    return matrice,solutions


def createCase(figures):
    vector = np.matrix([0,0,0,0,0,0,0])
    for figure in figures:
        position = figure['position']
        shape = dicoFig[figure['type']]
        size = figure['size']
        orientation = figure['orientation']
        color = figure['color']
        couleur = int(color[1:],16)
        fill = int(figure['fill'])

        v = np.matrix([shape,position['x'],position['y'],size,orientation,couleur,fill])
        vector = np.concatenate((vector,v),axis=0)

    vector = np.delete(vector ,0,0)
    return vector
