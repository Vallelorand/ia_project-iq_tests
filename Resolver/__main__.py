import Resolver.jsonToMatrix as jm
import Resolver.MatrixToAnswer as mta
import numpy as np
import sys

def findIndexSolutionByAI(json, isRawData=False) :
  if isRawData:
    matrice,solutions = jm.readJson("", json)
  else:
    matrice,solutions = jm.readJson(json)
  answerU,answerB = mta.buildAnswer(matrice,solutions)
  result1 = mta.chooseBestSolutions(solutions,answerU)
  result2 = mta.chooseBestSolutions(solutions,answerB)
  return result1,result2

if __name__ == '__main__' :
  if len(sys.argv)<2:
    print("× Veuillez spécifier le chemin d'entré du JSON.")
    exit(2)
  else :
    resultU, resultB = findIndexSolutionByAI(sys.argv[1])
    print("✓ Solution trouvée pour UnaryRPM : " + str(resultU))
    print("✓ Solution trouvée pour BinaryRPM : " + str(resultB))