import numpy as np

def buildAnswer(matrix3D,solutions) -> list :
	''' Return a vector wich represente an possible answer of the RPM in matrix3D'''

	# Trouve les différences entre les cases de la première ligne
	differences = __findDifferences(matrix3D[0],matrix3D[2])
	# print(differences)

	# Trouve les indices des figures impactées par les relations 3e ligne
	indicesFigures = __findIndicesFigure(matrix3D[6],matrix3D[7])
	# print(indicesFigures)

	# Génère une solution pour les RPM unaires
	resultU = __resolveUnary(matrix3D[6],differences,indicesFigures)
	# print(result)

	indicesFigures = __findIndicesFigure(matrix3D[0],matrix3D[2])
	# print(indicesFigures)
	# Génère une solution pour les RPM binaires
	resultB = __resolveBinary(matrix3D[6],matrix3D[7],differences,indicesFigures)
	# print(result)
	
	return resultU,resultB

def chooseBestSolutions(solutions,answer) -> int:
	''' Return the index of the solution which is the more like the answer'''
	diff = np.array([50,50,50,50,50,50,50,50])
	for i in range(len(solutions)) :
		if solutions[i].shape == answer.shape :
			diff[i] = np.sum(abs(abs(solutions[i]) - abs(answer)))

	indice = np.argmin(diff)
	return indice

def __findDifferences(case1,case3) -> list :
	result = np.zeros(7)
	# Pour chaque figure de la case 1
	for i in range(min(len(case1),len(case3))) :
		tmp = np.zeros(7)
		# Pour chaque carac de la figure
		for j in range(7) :
			#	Calcul de la différence avec l'homologue case1
			tmp[j] = case3[i][j] - case1[i][j]

		# Si différence non nulle, on sauvegarde
		if not np.array_equal(tmp,np.zeros(7)) :
			result = tmp
	return result

def __resolveUnary(case,differences,indexFigure) -> list :
	result = np.empty((len(case),7))
	for i in range(len(case)) :
		for j in range(7) :
			if i in indexFigure :
				result[i][j] = case[i][j] + differences[j]
			else :
				result[i][j] = case[i][j]
	return result.astype(int)

def __resolveBinary(case1,case2,differences,indexFigure) -> list:
	result = np.empty((len(case1),7))
	for i in range(len(case1)) :
		for j in range(7) :
			if i in indexFigure and differences[j] != 0 :
				result[i][j] = case1[i][j] + case2[i][j]
			else :
				result[i][j] = case1[i][j]
	return result

def __findIndicesFigure(case1,case2) -> list :
	result = []
	for i in range(min(len(case1),len(case2))) :
		for j in range(7) :
			if case1[i][j] != case2[i][j] :
				result.append(i)
				break
		
	return result
