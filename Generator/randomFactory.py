import Generator.Relations.UnaryRelation as ur
import Generator.Relations.BinaryRelation as br
from Generator.RPM import Case
from Generator.Figures import Circle,Line,Square,Triangle,Nulle
import random
import numpy as np

##-- Encyclopédie
URELATIONS = [ur.crossCase,ur.moveHorizontally,ur.moveVertically,ur.modifySize,ur.rotation]
BRELATIONS = [br.sumHorizontalPosition, br.sumVerticalPosition, br.sumOrientation, br.sumPosition, br.exclusiveDisjunctionShape,br.conjunctionShape,br.disjunctionShape]
FIGURES = [Circle.Circle, Line.Line, Square.Square, Triangle.Triangle]
COLORS = ["#FF33FF","#1536DB","#F25905","#02BA0F","#CC2626","#14D2B2","#8934D0"] #Rose,Bleu,Orange,Vert,Rouge,Cian,Magenta
##-- 

def buildRandomCase(nbFigure) :
	"""return a random case composed of different random figures"""
	result = Case.Case()
 
	for i in range(nbFigure) :
		#Tirage aléatoire des différents éléments
		filled = bool(random.getrandbits(1)) # remplie ou non
		colored = COLORS[random.randint(1,len(COLORS)-1)] #Couleur
		sized = random.randint(10,30) # Taille entre 10 et 30 pixels
		oriented = random.randint(0,180) # Orientation entre 0 et 360°
		indiceFigure = random.randint(0,len(FIGURES)-1) # Type de figure
		typeFigure = FIGURES[indiceFigure]
		Xpos, Ypos = __placeCorrectFigure(typeFigure, sized) #Position dépend de la taile et du type

		#Construction de la figure
		newFigure = typeFigure((Xpos,Ypos),filled,sized,oriented,colored)
		#Ajout à la case
		result.addFigure(newFigure)

	return result

def __placeCorrectFigure(typeF, size) :
	if (typeF == Circle.Circle) or (typeF == Triangle.Triangle) or (typeF == Line.Line) :
		x = random.randint(int(size/2),int(50-size/2))
		y = random.randint(int(size/2),int(50-size/2))
	elif typeF == Square.Square :
		x = random.randint(0,50-size)
		y = random.randint(0,50-size)
	else :
		x,y = 0,0 
		print(str(typeF) + " : probleme type")
	return (x,y)

def prepareUnaryRandomRPM() :
	"""build a random RPM with unary relations and three cases randomly generated
	then choose the params of the relations randomly"""
	# On pioche le nombre de figures
	nbFig = random.randint(1,3) # 1 à 3 figures
	cases = []
	cases.append(buildRandomCase(nbFig))
	cases.append(buildRandomCase(nbFig))
	cases.append(buildRandomCase(nbFig))

	# On pioche les relations
	nbRelations = random.randint(1,3) # 1 à 3 relations
	relations = []
	for i in range(nbRelations) :
		relations.append(URELATIONS[random.randint(0,len(URELATIONS)-1)])
	# Tirage des paramètres
	params = np.random.randint(1,10,len(relations)).tolist()
	
	return (cases,relations,params)

def prepareBinaryRandomRPM() :
	"""build a random RPM with binary relations and six cases randomly generated
	then choose the params of the relations randomly"""
	# On pioche le nombre de figures ligne 1
	nbFig1 = random.randint(1,3) # 1 à 3 figures
	# On pioche le nombre de figures ligne 2
	nbFig2 = random.randint(1,3) # 1 à 3 figures
	# On pioche le nombre de figures ligne 3
	nbFig3 = random.randint(1,3) # 1 à 3 figures
	cases = []
	cases.append(buildRandomCase(nbFig1))
	cases.append(buildRandomCase(nbFig1))
	cases.append(buildRandomCase(nbFig2))
	cases.append(buildRandomCase(nbFig2))
	cases.append(buildRandomCase(nbFig3))
	cases.append(buildRandomCase(nbFig3))

	# On pioche les relations
	nbRelations = random.randint(1,1) # 1 relations
	relations = []
	for i in range(nbRelations) :
		relations.append(BRELATIONS[random.randint(0,len(BRELATIONS)-1)])
	# Tirage des paramètres
	params = np.random.randint(1,10,len(relations)).tolist()
	
	return (cases,relations,params)