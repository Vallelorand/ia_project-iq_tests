from Generator.Figures.Figure import Figure

class Nulle(Figure) :
	def __init__(self):
		super().__init__((0,0), True, 0, 0, '')
	
	def toJson(self) :
		return { "type" : "Nulle",
		"position" : { "x" : 0, "y" : 0 },
		"fill" : True,
		"size" : 0,
		"orientation" : 0,
		"color" : '#000000' }
	
	def isValid(self) :
		return True
