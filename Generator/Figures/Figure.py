class Figure :

	def __init__(self, position, fill, size, orientation, color) :
		self.position = position
		self.fill = fill
		self.size = size
		self.orientation = orientation
		self.color = color

	def getPosition(self) :
		return self.position

	def isFill(self) :
		return self.fill

	def getSize(self) :
		return self.size

	def getOrientation(self) :
		return self.orientation

	def getColor(self) :
		return self.color

	def toJson(self) :
		pass

	def getMargin(self) :
		pass

	def __eq__(self, f) :
		result = type(self) == type(f)
		result &= self.getOrientation() == f.getOrientation()
		result &= self.getPosition() == f.getPosition()
		result &= self.getSize() == f.getSize()
		result &= self.isFill() == f.isFill()
		result &= self.getColor() == f.getColor()
		return result

	def copy(self) :
		return type(self)(self.position, self.fill, self.size, self.orientation, self.color)

	## verifie que la figure est valide en regardant ses coordonnées
	def isValid(self) :
		pass
		
	def __str__(self):
	 return type(self).__name__ +"(pos:"+ str(self.position) + ",fill:"+ str(self.fill) + ",size:"+ str(self.size) +",orien:"+ str(self.orientation) +",color:"+ self.color +")"




