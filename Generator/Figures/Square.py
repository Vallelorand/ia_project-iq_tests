from Generator.Figures.Figure import Figure

class Square(Figure) :
	def __init__(self, position, fill, size, orientation, color):
		super().__init__(position, fill, size, orientation, color)

	def toJson(self) :
		return { "type" : "Square",
		"position" : { "x" : self.position[0], "y" : self.position[1] },
		"fill" : self.fill,
		"size" : self.size,
		"orientation" : self.orientation,
		"color" : self.color }

	def getMargin(self):
	 	return 1, self.size
	
	def isValid(self) :
		return (((self.position[0] + self.size) <= 50) and ((self.position[1] + self.size) <= 50) and ((self.position[0]) >= 0) and ((self.position[1]) >= 0) and (self.size > 0))

