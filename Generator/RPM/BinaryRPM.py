import json
import random
from Generator.RPM.Case import Case
import Generator.Relations.BinaryRelation as br
import numpy as np
import Generator.randomFactory as rf


class BinaryRPM() :
	""" RPM composed by one binary relation,
	 8 cases as questions and 8 cases as answers, 
	 only one answer is the good one
	"""


	def __init__(self,cases, relations,params) :# Need 6 cases and 1 binary relation
		self.cases = [Case(), Case(), Case(), Case(), Case(), Case(), Case(), Case()] #list of 8 cases -> questions
		self.reponses = [] #list of 8 cases -> answers
		self.cases[0] = cases[0] #UpperLeft
		self.cases[1] = cases[1] #UpperCenter
		self.cases[3] = cases[2] #MiddleLeft 
		self.cases[4] = cases[3] #MiddleCenter
		self.cases[6] = cases[4] #BottomLeft
		self.cases[7] = cases[5] #BottomCenter 
		self.relations = relations #liste des relations
		self.params = params #liste des paramètres

	def generateRPM(self) :
		""" generate 3 others cases thanks to the relation
		then generate 7 fake answers. shuffle the array of answers
		and save the positon of the correct answers in self.indiceSolution
		"""
		#Generate lines
		self.__setupLine(1)
		self.__setupLine(2)
		self.__setupLine(3)

		#Generate fake answers
		self.__generateFakeAnswers()

		#Shuffle the answers
		goodSolution = self.reponses[0]
		random.shuffle(self.reponses)

		#Find the index of the good answer
		for i in range(len(self.reponses)) :
			if goodSolution.__eq__(self.reponses[i]) :
				self.indiceSolution = i


	def __setupLine(self,numberLine) :
		'''Manage the setup of the numberLine. With the first case, 
		generate the other cases of the line and place them at the right place'''
		#Initialisation
		if numberLine == 1 :
			line = 0
		elif numberLine == 2 :
			line = 3
		elif numberLine == 3 :
			line = 6
		else :
			raise Exception("nombre ligne incorrect !")

		#Generation des deux cases de la line
		generatedCase = self.__applyRelationsOnCase(self.cases[line], self.cases[line+1])

		#Ajout de la case de droite
		if numberLine != 3 :
			self.cases[line+2] = generatedCase
		else : #Si dernière ligne alors dernière case est la solution
			self.reponses.append(generatedCase)
		

	def __generateFakeAnswers(self) :
		'''Generate fake answers. 
		Apply the relations on bottom-left case while we have 7 answers  different of the correct answers.'''
		#Tant que nous n'avons pas les 8 reponses
		counter = 0
		while len(self.reponses) < 8 :
			if counter >= 1000 :
				print("NOTE: génération de figures similaires impossible, passage en mode aleatoire")
				while len(self.reponses) < 8 :
					resultat = rf.buildRandomCase(random.randint(1,3))
					self.reponses.append(resultat)
			else :
				generatedCase = self.__applyRandomRelationsOnCase(self.cases[6], self.cases[7])
				#Si la case générée n'est pas égale à la bonne réponse on l'ajoute
				if generatedCase not in self.reponses :
					self.reponses.append(generatedCase)
			counter = counter+1


	def __applyRelationsOnCase(self,case1, case2) :
		'''Apply all the relations randomly on the figures of the case.'''
		
		generatedCase = case1.clone()
		#Tirage de la figure à modifier dans la case
		indice1 = random.randint(0,min(case1.nombreFigure()-1,case2.nombreFigure()-1))

		#On applique les relations
		fig1 = br.composedRelation(case1.getFigure(indice1),case2.getFigure(indice1),self.relations,self.params)
		generatedCase.replace(indice1,fig1)

		return generatedCase

	def __applyRandomRelationsOnCase(self,case1, case2) :
		'''Apply all the relations randomly on the figures of the case.'''
		
		generatedCase = Case()
		#On pioche un nombre de figure à modifier dans la case
		nombre = random.randint(1,case1.nombreFigure())
		counter = 0

		#On pioche 1 relation et 1 param
		relations = [rf.BRELATIONS[random.randint(0,len(rf.BRELATIONS)-1)]]
		params = [random.randint(1,10)]

		
		for i in case1.figures :
			#On applique la relation
			if counter<nombre :
				#Tirage des figures
				indice1 = random.randint(0,case1.nombreFigure()-1)
				indice2 = random.randint(0,case2.nombreFigure()-1)
				#On applique l'ensemble des relations
				fig1 = br.composedRelation(case1.getFigure(indice1),case2.getFigure(indice2),relations,params)
				generatedCase.addFigure(fig1)
				counter = counter+1
			#On ne modifie pas les figures 
			else :
				generatedCase.addFigure(i)

		return generatedCase

	def generateJson(self,path=None) :
		""" from all of the datas contained by the rpm, generate a JSON file in the path given.
		prerequisite : generateRPM() method must be called before.
		"""
		result = {}
		result["upperLeft"] = self.cases[0].toJson()
		result["upperCenter"] = self.cases[1].toJson()
		result["upperRight"] = self.cases[2].toJson()
		result["middleLeft"] = self.cases[3].toJson()
		result["middleCenter"] = self.cases[4].toJson()
		result["middleRight"] = self.cases[5].toJson()
		result["bottomLeft"] = self.cases[6].toJson()
		result["bottomCenter"] = self.cases[7].toJson()
		result["solutions"] = [i.toJson() for i in self.reponses]
		result["solutionIndex"] = self.indiceSolution
		if path:
			with open(path, 'w') as outfile:
				json.dump(result, outfile, indent=2)
		else:
			return result