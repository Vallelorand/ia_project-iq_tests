import json
import random
from Generator.RPM.Case import Case
import Generator.Relations.UnaryRelation as ur
import numpy as np
import Generator.randomFactory as rf

class UnaryRPM() :
	""" RPM composed by one unary relation,
	 8 cases as questions and 8 cases as answers, 
	 only one answer is the good one
	"""
	
	def __init__(self,cases, relations,params) :# Need 3 cases and 1 unary relation
		self.cases = [Case(), Case(), Case(), Case(), Case(), Case(), Case(), Case()] #list of 8 cases -> questions
		self.reponses = [] #list of 8 cases -> answers

		self.cases[0] = cases[0] #UpperLeft
		self.cases[3] = cases[1] #MiddleLeft
		self.cases[6] = cases[2] #BottomLeft
		self.relations = relations #liste de relations
		self.params = params #liste de paramètres

	def generateRPM(self) :
		""" generate 6 others cases thanks to the relation
		then generate 7 fake answers. shuffle the array of answers
		and save the positon of the correct answers in self.indiceSolution
		"""
		#Generate lines
		self.__setupLine(1)
		self.__setupLine(2)
		self.__setupLine(3)

		#Generate fake answers
		self.__generateFakeAnswers()

		#Shuffle the answers
		goodSolution = self.reponses[0]
		random.shuffle(self.reponses)

		#Find the index of the good answer
		for i in range(len(self.reponses)) :
			if goodSolution.__eq__(self.reponses[i]) :
				self.indiceSolution = i

	def generateJson(self,path=None) :
		""" from all of the datas contained by the rpm, generate a JSON file in the path given.
		prerequisite : generateRPM() method must be called before.
		"""
		assert len(self.reponses) == 8, "Answers not generated"
		result = {}
		result["upperLeft"] = self.cases[0].toJson()
		result["upperCenter"] = self.cases[1].toJson()
		result["upperRight"] = self.cases[2].toJson()
		result["middleLeft"] = self.cases[3].toJson()
		result["middleCenter"] = self.cases[4].toJson()
		result["middleRight"] = self.cases[5].toJson()
		result["bottomLeft"] = self.cases[6].toJson()
		result["bottomCenter"] = self.cases[7].toJson()
		result["solutions"] = [i.toJson() for i in self.reponses]
		result["solutionIndex"] = self.indiceSolution
		if path is not None:
			with open(path, 'w') as outfile:
				json.dump(result, outfile, indent=2)
		else:
			return result
		
### --- Private methods --- ###
	def __setupLine(self,numberLine) :
		'''Manage the setup of the numberLine. With the first case, 
		generate the other cases of the line and place them at the right place'''
		#Initialisation
		if numberLine == 1 :
			line = 0
		elif numberLine == 2 :
			line = 3
		elif numberLine == 3 :
			line = 6
		else :
			raise Exception("nombre ligne incorrect !")

		#Generation des deux cases de la line
		generatedCase1, generatedCase2 = self.__applyRelationsOnCase(self.cases[line])

		#Ajout de la case du milieu
		self.cases[line+1] = generatedCase1
		#Ajout de la case de droite
		if numberLine != 3 :
			self.cases[line+2] = generatedCase2
		else : #Si dernière ligne alors dernière case est la solution
			self.reponses.append(generatedCase2)
		
	def __generateFakeAnswers(self) :
		'''Generate fake answers. Apply the relations on bottom-left case while we have 7 answers  different of the correct answers.'''
		counter = 0
		#Tant que nous n'avons pas les 8 reponses
		while len(self.reponses) < 8 :
			if counter >= 1000 :
				print("NOTE: génération de figures similaires impossible, passage en mode aleatoire")
				while len(self.reponses) < 8 :
					nbFig = random.randint(1,3) # 1 à 3 figures
					resultat = rf.buildRandomCase(nbFig)
					self.reponses.append(resultat)
			else :
				generatedCase = self.__applyRandomRelationsOnCase(self.cases[6])
				#Si la case générée n'est pas égale à la bonne réponse on l'ajoute
				if generatedCase not in self.reponses :
					self.reponses.append(generatedCase)
			counter = counter+1

	def __applyRelationsOnCase(self,case) :
		'''Apply all the relations randomly on the figures of the case.'''
		generatedCase1 = case.clone()
		generatedCase2 = case.clone()
		#On pioche un nombre de figure à modifier dans la case
		nombre = random.randint(1,case.nombreFigure())
		#Pour chacune de ces figures, on applique l'ensemble des relations
		for i in range(nombre) :
			#Tirage de la figure et des params
			indice = random.randint(0,case.nombreFigure()-1)

			#On applique l'ensemble des relations
			fig1, fig2 = ur.composedRelation(case.getFigure(indice),self.relations,self.params)
			generatedCase1.replace(indice,fig1)
			generatedCase2.replace(indice,fig2)
				
		return generatedCase1, generatedCase2

	def __applyRandomRelationsOnCase(self,case) :
		'''Apply all the relations randomly on the figures of the case.'''
		generatedCase2 = case.clone()
		#On pioche un nombre de figure à modifier dans la case
		nombre = random.randint(1,case.nombreFigure())

		#On pioche 1 relation et 1 param
		relations = [rf.URELATIONS[random.randint(0,len(rf.URELATIONS)-1)]]
		params = [random.randint(1,10)]

		#On applique la relation sur les figures piochée
		for i in range(nombre) :
			#Tirage de la figure et du params
			indice = random.randint(0,case.nombreFigure()-1)
			
			#On applique les relations
			fig1, fig2 = ur.composedRelation(case.getFigure(indice),relations,params)
			generatedCase2.replace(indice,fig2)
				
		return generatedCase2
