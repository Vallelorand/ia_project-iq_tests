class Case() :
	
	def __init__(self):
		self.figures = list()

	def clone(self) :
		case = Case()
		for i in range(len(self.figures)) :
			case.addFigure(self.figures[i].copy())
		return case		
	
	def addFigure(self, figure):
		self.figures.append(figure)
	
	
	def getFigure(self, index):
		assert index < len(self.figures), "Index out of bound."
		return self.figures[index]

	def nombreFigure(self):
			return len(self.figures)
	
	def replace(self, index ,newF):
		assert index < len(self.figures), "Index out of bound."
		self.figures[index] = newF

	def __eq__(self, case) :
		if type(self) != type(case) :
			return False
		if self.nombreFigure() != case.nombreFigure() :
			return False

		for i in range(len(self.figures)) :
			if not self.figures[i].__eq__(case.getFigure(i)) :
				return False
		return True

	def __str__(self):
		result = "["
		for i in self.figures :
			result = result + str(i) + ", "
		result += "]"
		return result

	def toJson(self):
		return {"figures" : [ 
		i.toJson() for i in self.figures ]}