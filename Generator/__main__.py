from Generator.RPM import UnaryRPM, BinaryRPM
from Generator.RPM.Case import Case
from Generator.Figures.Triangle import Triangle
from Generator.Figures.Square import Square
from Generator.Figures.Circle import Circle
from Generator.Figures.Line import Line
import Generator.Relations.UnaryRelation as ur
import Generator.Relations.BinaryRelation as br
import Generator.randomFactory as rf
import sys
import random

def testUnary():
	#Creation de la case 1
	case1 = Case()
	square = Square((30,30), False, 15, 0, "#14D2B2")
	circle = Circle((15,15), True, 15, 0, "#F25905")
	case1.addFigure(square)
	case1.addFigure(circle)

	#Creation de la case 2
	case2 = Case()
	triangle = Triangle((30,17), False, 30, 0, "#FF33FF")
	circle = Circle((25,25), False, 20, 0, "#F25905")
	case2.addFigure(triangle)
	case2.addFigure(circle)

	#Creation de la case 3
	case3 = Case()
	circle = Circle((20,20), False, 20, 0, "#1111AA")
	square = Square((15,10), True, 15, 50, "#CC2626")
	case3.addFigure(circle)
	case3.addFigure(square)
	

	#Choix de la relation
	relations = [ur.moveHorizontally]
	params = [6]
	#Creation RPM
	return UnaryRPM.UnaryRPM([case1, case2, case3], relations,params)

def testBinary():
	
	#Creation de la case 1
	case1 = Case()
	square = Square((28,10),True, 20, 0, "#CC2626")
	line = Line((20,40), False, 20, 60, "#02BA0F")
	case1.addFigure(line)
	case1.addFigure(square)

	#Creation de la case 2
	case2 = Case()
	square = Square((30,15),True, 20, 0, "#CC2626")
	line = Line((12,12), False, 20, 60, "#02BA0F")
	case2.addFigure(line)
	case2.addFigure(square)


	#Creation de la case 3
	case3 = Case()
	square = Square((5,10),True, 12, 0, "#14D2B2")
	circle = Circle((25,15), False, 22, 0, "#AAAAAA")
	case3.addFigure(square)
	case3.addFigure(circle)

	#Creation de la case 4
	case4 = Case()
	square = Square((10,17),True, 12, 0, "#14D2B2")
	circle = Circle((30,15), False, 22, 0, "#AAAAAA")
	case4.addFigure(square)
	case4.addFigure(circle)

	#Creation de la case 5
	case5 = Case()
	square = Square((5,5),True,15,4,"#1536DB")
	circle = Circle((20,30), False, 10, 0, "#1111AA")
	case5.addFigure(square)
	case5.addFigure(circle)

	#Creation de la case 6
	case6 = Case()
	square = Square((30,5),True,15,4,"#1536DB")
	circle = Circle((40,40), False, 10, 0, "#1111AA")
	case6.addFigure(square)
	case6.addFigure(circle)

	#Choix de la relation
	relations = [br.sumVerticalPosition]
	params = [4]

	#Creation RPM
	return BinaryRPM.BinaryRPM([case1, case2, case3,case4,case5,case6], relations,params)

def generateRandomRPM() : 
	choice = random.randint(1,2)
	if choice == 1 :
		print("Fabrication d'une RPM unaire...")
		(cases,relations,params) = rf.prepareUnaryRandomRPM()
		return UnaryRPM.UnaryRPM(cases,relations,params)
	else :
		print("Fabrication d'une RPM binaire...")
		(cases,relations,params) = rf.prepareBinaryRandomRPM()
		return BinaryRPM.BinaryRPM(cases,relations,params)

if __name__ == '__main__' :
	rpm = []
	if len(sys.argv)<2:
		print("× Veuillez spécifier le chemin de sortie du JSON.")
		exit(2)
	elif len(sys.argv) == 3 :
		if sys.argv[2] == "unary" :
			rpm = testUnary()
		elif sys.argv[2] == "binary" :
			rpm = testBinary()
		else : 
			print("Deuxième paramètre incorrect")
			exit(2)
	else :
		rpm = generateRandomRPM()
	
	rpm.generateRPM()
	rpm.generateJson(sys.argv[1])
	print("✓ Fichier " + sys.argv[1] + " généré.")