TAILLE = 50

def crossCase(figure,param):
	r11, r12 = moveHorizontally(figure,param)
	r21, r22 = moveVertically(figure,param)

	return type(figure)((r11.getPosition()[0], r21.getPosition()[1]),
				figure.isFill(),figure.getSize(), figure.getOrientation(), figure.getColor()), \
			type(figure)((r12.getPosition()[0], r22.getPosition()[1]),
				figure.isFill(),figure.getSize(),figure.getOrientation(),figure.getColor())

def moveHorizontally(figure,param):
	newFigure1 = type(figure)
	newFigure2 = type(figure)
	(x, y) = figure.getPosition()
	
	#FORMULE
	if param <=5 :
		newX = x + (TAILLE - x - figure.getMargin()[1])//2
		newX2 = x + (TAILLE - x - figure.getMargin()[1])
	else :
		newX = x - (x - figure.getMargin()[0])//2
		newX2 = x - (x - figure.getMargin()[0])

	return newFigure1((newX, y),figure.isFill(), figure.getSize(), figure.getOrientation(), figure.getColor()), \
		   newFigure2((newX2, y), figure.isFill(),figure.getSize(), figure.getOrientation(),figure.getColor())

def moveVertically(figure,param):
	newFigure1 = type(figure)
	newFigure2 = type(figure)
	(x, y) = figure.getPosition()

	#FORMULE
	if param <=5 :
		newY = y + (TAILLE - y - figure.getMargin()[1])//2
		newY2 = y + (TAILLE - y - figure.getMargin()[1])
	else :
		newY = y - (y - figure.getMargin()[0])//2
		newY2 = y - (y - figure.getMargin()[0])

	return newFigure1((x, newY), figure.isFill(), figure.getSize(),figure.getOrientation(), figure.getColor()), \
		   newFigure2((x, newY2),figure.isFill(),figure.getSize(),figure.getOrientation(), figure.getColor())


def rotation(figure,param):
	#FORMULE
	newOrien = (figure.getOrientation()+param/2) %360
	newOrien2 = (figure.getOrientation()+param) %360

	return type(figure)(figure.getPosition(),figure.isFill(),figure.getSize(),
					  newOrien,figure.getColor()), \
		   type(figure)(figure.getPosition(),figure.isFill(),figure.getSize(),
					  newOrien2,figure.getColor())


def modifySize(figure,param) :
	#FORMULE
	newSize =  figure.getSize() +  (TAILLE - figure.getSize() - (figure.getMargin()[1]+figure.getMargin()[0]))//2
	newSize2 = figure.getSize() +  (TAILLE - figure.getSize() - (figure.getMargin()[1]+figure.getMargin()[0]))

	return type(figure)(figure.getPosition(),figure.isFill(),newSize,figure.getOrientation(), figure.getColor()), \
		   type(figure)(figure.getPosition(), figure.isFill(),newSize2, figure.getOrientation(),figure.getColor())


def composedRelation(figure,methods,params):
	FigureGauche = figure
	FigureDroite = figure
	for m,p in zip(methods,params) :
		FigInterestA,inutileA = m(FigureGauche,p)
		inutileB,FigInterestB = m(FigureDroite,p)
		FigureGauche = FigInterestA
		FigureDroite = FigInterestB
	return FigureGauche,FigureDroite

# Move a figure horizontally to the opposite side of the grid. Returns two Figures which matches with the second and the
# third figure of a line in a RPM. The Figure move to the middle first then spread from the middle as far as it was originally