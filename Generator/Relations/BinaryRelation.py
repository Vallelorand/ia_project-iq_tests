from Generator.Figures.Nulle import Nulle
from Generator.Figures.Square import Square
from Generator.Figures.Circle import Circle
from Generator.Figures.Triangle import Triangle
from Generator.Figures.Line import Line

TAILLE = 50

def sumOrientation(f1,f2,params) :
  '''return f1 figure with a orientation egals to the orientation sum of f1 and f2.
  if params <= 50 it's a sum otherwise it's a substraction'''
  #FORMULES
  if params <= 5 :
    newOrientation =  (f1.getOrientation() + f2.getOrientation()) % 360
  else :
    newOrientation =  (f1.getOrientation() - f2.getOrientation()) % 360
  
  return type(f1)(f1.getPosition(),f1.isFill(),f1.getSize(),newOrientation,f1.getColor())

def sumSize(f1,f2,params) :
  '''return f1 figure with a size egals to the size sum of f1 and f2.
  if params <= 5 it's a sum otherwise it's a substraction'''
  (x,y) = f1.getPosition()
  borderCloser = min(x*2,y*2,(TAILLE-x)*2,(TAILLE-y)*2) 
  #FORMULES
  if params <= 5 :
    newSize = (f1.getSize() + f2.getSize()) % (borderCloser)
  else :
    newSize = (f1.getSize() - f2.getSize()) % (borderCloser)
  
  return type(f1)(f1.getPosition(),f1.isFill(),newSize,f1.getOrientation(),f1.getColor())

def sumHorizontalPosition(f1,f2,params) :
  '''return f1 figure with a X position egals to the sum of f1 and f2 X position.
  if params <= 5 it's a sum otherwise it's a substraction'''
  #FORMULES
  if params <= 5 :
    newX = ((f1.getPosition()[0]-f1.getMargin()[0] + (f2.getPosition()[0]-f2.getMargin()[0])) % (TAILLE - f1.getMargin()[1]))
  else :
    newX = ((f1.getPosition()[0]-f1.getMargin()[0] - (f2.getPosition()[0]-f2.getMargin()[0])) % (TAILLE - f1.getMargin()[1]))

  newX = f1.getMargin()[0] if newX < f1.getMargin()[0] else newX

  return type(f1)((newX,f1.getPosition()[1]),f1.isFill(),f1.getSize(),f1.getOrientation(),f1.getColor())

def sumVerticalPosition(f1,f2,params) :
  '''return f1 figure with a Y position egals to the sum of f1 and f2 Y position.
  if params <= 5 it's a sum otherwise it's a substraction'''
  #FORMULES
  if params <= 5 :
    newY = ((f1.getPosition()[1]-f1.getMargin()[0] + (f2.getPosition()[1]-f2.getMargin()[0])) % (TAILLE - f1.getMargin()[1]))
  else :
    newY = ((f1.getPosition()[1]-f1.getMargin()[0] - (f2.getPosition()[1]-f2.getMargin()[0])) % (TAILLE - f1.getMargin()[1]))
  
  newY = f1.getMargin()[0] if newY < f1.getMargin()[0] else newY

  return type(f1)((f1.getPosition()[0],newY), f1.isFill(),f1.getSize(), f1.getOrientation(), f1.getColor())

def sumPosition(f1,f2,params) :
  '''return f1 figure with a Y position egals to the sum of f1 and f2 Y position.'''
  r1 = sumHorizontalPosition(f1,f2,params)
  r2 = sumVerticalPosition(f1,f2,params)

  return type(f1)((r1.getPosition()[0],r2.getPosition()[1]), f1.isFill(), f1.getSize(), f1.getOrientation(), f1.getColor())

def conjunctionStrict(f1,f2,params) :
  '''return f1 if f1 AND f2 are equals.'''
  if f1 == f2 :
    return f1
  else :
    return Nulle()

def conjunctionShape(f1,f2,params) :
  '''return f1 if f1 AND f2 have the shape in params.'''
  shape = __paramToShape(params)
  newFigure = type(f1)
  if type(f1) == shape and type(f2) == shape :
    return f1
  else :
    return Nulle()

def disjunctionShape(f1,f2,params) :
  '''return a f1 as square if f1 OR f2 have the shape in params.'''  
  shape = __paramToShape(params)
  if type(f1) == shape :
    return f1
  elif type(f2) == shape :
    return f2
  else :
    return Nulle()

def exclusiveDisjunctionShape(f1,f2,params) :
  '''return a f1 as square if f1 XOR f2 have the shape in params.'''  
  shape = __paramToShape(params)
  if (type(f1) == shape) and (not type(f2) == shape) :
    return f1
  elif (type(f2) == shape) and (not type(f1) == shape) :
    return f2
  else :
    return Nulle()

def composedRelation(f1,f2,methods,params):
  result = f1.copy()
  for m,p in zip(methods,params) :
      result = m(result,f2,p)
  return result

def __paramToShape(params) :
  """[1;3] -> Circle | [4;5] -> Square | [6;8] -> Triangle | [9;10] -> Line"""
  if params <= 3 :
    return Circle
  elif params >3 and params <= 5 :
    return Square
  elif params >5 and params <= 8 :
    return Triangle
  else :
    return Line


