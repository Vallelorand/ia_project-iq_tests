import unittest
from  Generator.Figures.Circle import Circle
from  Generator.Figures.Triangle import Triangle
import Generator.Relations.BinaryRelation as br
import Generator.Relations.UnaryRelation as ur

class TestComposedRelations(unittest.TestCase):
 
  def test_UnaryComposedRelation(self):
    relations = [ur.moveHorizontally,ur.modifySize]
    params = [5,5]
    c1 = Circle((10,10),True,20,15,"#AAAAAA")
    (c2,c3)  = ur.composedRelation(c1,relations,params)
    #c2 check
    self.assertEqual((25,10), c2.getPosition()) # 10 + (50 - 10 - 20//2)//2
    self.assertEqual(25, c2.getSize()) # 20 + (50 - 20 - 20)//2
    #c3 check
    self.assertEqual((40,10), c3.getPosition()) # 10 + (50 - 10 - 20//2)
    self.assertEqual(30, c3.getSize()) # 20 + (50 - 20 - 20)

  def test_BinaryComposedRelation(self):
    relations = [br.sumHorizontalPosition,br.sumOrientation]
    params = [5,5]
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    c2 = Triangle((20,4),False,16,180,"#AAAAAA")
    c  = br.composedRelation(c1,c2,relations,params)
    #c check
    self.assertEqual((17,10), c.getPosition()) #10-5 + 20-8
    self.assertEqual(195, c.getOrientation())

