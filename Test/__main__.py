import unittest
from Test.TestFigures import TestFigures
from Test.TestBinaryRelations import TestBinaryRelations
from Test.TestUnaryRelations import TestUnaryRelations
from Test.TestCases import TestCases
from Test.TestComposedRelations import TestComposedRelations

unittest.main()