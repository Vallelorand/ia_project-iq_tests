import unittest
from  Generator.Figures.Circle import Circle
from  Generator.Figures.Square import Square
from  Generator.Figures.Triangle import Triangle
import Generator.Relations.BinaryRelation as br
import Generator.Relations.UnaryRelation as ur

class TestFigures(unittest.TestCase):

  def test_creationCircle(self):
    c1 = Circle((10,10),True,10,0,"#AAAAAA")
    self.assertEqual(type(c1), Circle)
    self.assertEqual(c1.getOrientation(), 0)

  def test_creationSquare(self):
    c1 = Square((10,10),True,10,0,"#AAAAAA")
    self.assertEqual(type(c1), Square)
    self.assertEqual(c1.getOrientation(), 0)

  def test_FigureEqual(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    c2 = Circle((10,10),True,10,15,"#AAAAAA")
    r1 = Triangle((10,10),True,10,15,"#AAAAAA")
    self.assertEqual(c1,c2)
    self.assertNotEqual(c1,r1)
    self.assertNotEqual(r1,c2)

  def test_FigureString(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    r1 = Triangle((0,8),False,1,1,"#FFFFFF")
    self.assertEqual("Circle(pos:(10, 10),fill:True,size:10,orien:15,color:#AAAAAA)",str(c1))


  def test_toJSON(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")

    expected = {
      "type": "Circle", 
      "position": {"x": 10, "y": 10}, 
      "fill": True, 
      "size": 10, 
      "orientation": 15,
      "color": "#AAAAAA",
    }
    self.assertEqual(expected,c1.toJson())

  def test_CircleValid(self):
    c1 = Circle((10,20),True,10,15,"#AAAAAA")
    self.assertTrue(c1.isValid())
    c2 = Circle((10,20),True,10,15,"#AAAAAA")
    self.assertTrue(c1.isValid())
    c1 = Circle((10,20),True,10,15,"#AAAAAA")
    self.assertTrue(c1.isValid())