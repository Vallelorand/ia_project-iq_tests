import json
import Generator.randomFactory as rf
import Resolver.jsonToMatrix as jm
import Resolver.MatrixToAnswer as mta
from Generator.RPM import UnaryRPM, BinaryRPM
import time

def isCorrect(result, path) :
  with open(path, 'r') as file:
    data = json.load(file)
  correctResult = data["solutionIndex"]

  return result == correctResult

def accuracyUnaryResolver(x) :
  results = []
  path = "JSON/accuracy.json"

  for i in range(x) :
    (cases,relations,params) = rf.prepareBinaryRandomRPM()
    rpm = BinaryRPM.BinaryRPM(cases,relations,params)
    rpm.generateRPM()
    rpm.generateJson(path)

    matrice,solutions = jm.readJson(path)
    answerU,answerB = mta.buildAnswer(matrice,solutions)
    result = mta.chooseBestSolutions(solutions,answerB)
    ok = isCorrect(result,path)
    results.append(ok)

  goodAnswer = [i for i in results if i != False]
  return float(len(goodAnswer)) / len(results)

if __name__ == "__main__" :
  accuracy = accuracyUnaryResolver(1000)
  print(accuracy)