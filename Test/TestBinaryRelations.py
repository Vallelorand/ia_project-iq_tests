import unittest
from  Generator.Figures.Circle import Circle
from  Generator.Figures.Triangle import Triangle
from Generator.Figures.Square import Square
from Generator.Figures.Nulle import Nulle
import Generator.Relations.BinaryRelation as br
import Generator.Relations.UnaryRelation as ur

class TestBinaryRelations(unittest.TestCase):
 
#--sumOrientation()
  def test_sumOrientation(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    c2 = Circle((10,10),True,10,90,"#AAAAAA")
    c3 = br.sumOrientation(c1,c2,0)
    self.assertEqual(c3.getOrientation(), 105)

  def test_subOrientation(self):
    c1 = Circle((10,10),True,10,300,"#AAAAAA")
    c2 = Circle((10,10),True,10,300,"#AAAAAA")
    c3 = br.sumOrientation(c1,c2,80)
    self.assertEqual(c3.getOrientation(), 0)

  def test_sumOrientationModulo(self):
    c1 = Circle((10,10),True,10,300,"#AAAAAA")
    c2 = Circle((10,10),True,10,300,"#AAAAAA")
    c3 = br.sumOrientation(c1,c2,0)
    self.assertEqual(c3.getOrientation(), 240)

#--sumSize()
  def test_sumSize(self):
    c1 = Circle((20,20),True,10,15,"#AAAAAA")
    c2 = Circle((20,20),True,10,90,"#AAAAAA")
    c3 = br.sumSize(c1,c2,0)
    self.assertEqual(c3.getSize(), 20) # 10 + 10

  def test_subSize(self):
    c1 = Circle((10,10),True,20,15,"#AAAAAA")
    c2 = Circle((10,10),True,5,90,"#AAAAAA")
    c3 = br.sumSize(c1,c2,8)
    self.assertEqual(c3.getSize(), 15) # 20 - 5

  def test_sumSizeModulo(self):
    c1 = Circle((20,25),True,34,15,"#AAAAAA")
    c2 = Circle((20,20),True,30,90,"#AAAAAA")
    c3 = br.sumSize(c1,c2,0)
    self.assertEqual(c3.getSize(), 24) # 34 + 30 %(40)
#--sumHorizontalPosition()
  def test_sumHorizontalPosition(self):
    c1 = Circle((5,2),True,5,6,"#AAAAAA")
    c2 = Circle((7,4),True,7,8,"#AAAAAA")
    c3 = br.sumHorizontalPosition(c1,c2,0) 
    self.assertEqual(c3.getPosition(), (6,2))# 5-2.5 + 7-3.5

  def test_subHorizontalPosition(self):
    c1 = Circle((10,15),True,5,6,"#AAAAAA")
    c2 = Circle((7,8),True,7,8,"#AAAAAA")
    c3 = br.sumHorizontalPosition(c1,c2,80)
    self.assertEqual(c3.getPosition(), (4,15)) # 10-2.5 - 7-3.5

  def test_sumHorizontalPositionModulo(self):
    c1 = Circle((49,2),True,5,6,"#AAAAAA")
    c2 = Circle((8,4),True,4,8,"#AAAAAA")
    c3 = br.sumHorizontalPosition(c1,c2,0)
    self.assertEqual(c3.getPosition(), (5,2)) #(49 -2.5) + (8-2) % (50 - 2.5)

#--sumVerticalPosition()
  def test_sumVerticalPosition(self):
    c1 = Circle((1,5),True,5,6,"#AAAAAA")
    c2 = Circle((3,7),True,7,8,"#AAAAAA")
    c3 = br.sumVerticalPosition(c1,c2,0)
    self.assertEqual(c3.getPosition(), (1,6)) #5-5/2 + 7-7/2

  def test_subVerticalPosition(self):
    c1 = Circle((1,10),True,5,6,"#AAAAAA")
    c2 = Circle((3,4),True,7,8,"#AAAAAA")
    c3 = br.sumVerticalPosition(c1,c2,10)
    self.assertEqual(c3.getPosition(), (1,7)) #10-2.5 - (4-3.5)

  def test_sumVerticalPositionModulo(self):
    c1 = Circle((1,47),True,6,6,"#AAAAAA")
    c2 = Circle((3,10),True,8,8,"#AAAAAA")
    c3 = br.sumVerticalPosition(c1,c2,0)
    self.assertEqual(c3.getPosition(), (1,3)) # (47-3) + (10-4) % (50-3) + [3]

#--sumPosition()
  def test_sumPosition(self):
    c1 = Circle((5,8),True,5,6,"#AAAAAA")
    c2 = Circle((7,9),True,7,8,"#AAAAAA")
    c3 = br.sumPosition(c1,c2,0)
    self.assertEqual(c3.getPosition(), (6,11)) #5-5/2 + 7-7/2, 8-5/2 + 9-7/2
  
  def test_subPosition(self):
    c1 = Circle((7,9),True,5,6,"#AAAAAA")
    c2 = Circle((4,4),True,7,8,"#AAAAAA")
    c3 = br.sumPosition(c1,c2,80)
    self.assertEqual(c3.getPosition(), (4,6)) #7-5/2 - (4-7/2), 9-5/2 - (4-7/2)

  def test_sumPositionModulo(self):
    c1 = Circle((30,31),True,5,6,"#AAAAAA")
    c2 = Circle((32,33),True,7,8,"#AAAAAA")
    c3 = br.sumPosition(c1,c2,0)
    self.assertEqual(c3.getPosition(), (8.5,10.5)) #30-2.5 +32-3.5 % (50-2.5) + [2.5], 31-2.5 +33-3.5 % (50-2.5) + [2.5]

#-- conjunction()
  def test_conjunctionStrict(self):
    c1 = Circle((10,10),True,20,90,"#AAAAAA")
    c2 = Circle((10,10),True,20,90,"#AAAAAA")
    c = br.conjunctionStrict(c1,c2,0)
    self.assertEqual(c1, c)
    c3 = Square((10,10),True,20,90,"#AAAAAA")
    c =  br.conjunctionStrict(c1,c3,0)
    self.assertEqual(Nulle(), c)

  def test_conjunctionShape(self):
    """[1;25] -> Circle | [26;50] -> Square | [51;75] -> Triangle | [76;100] -> Line"""
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    c2 = Circle((10,10),True,20,90,"#AAAAAA")
    c = br.conjunctionShape(c1,c2,5)
    self.assertEqual(c, Nulle())
    c = br.conjunctionShape(c1,c2,3)
    self.assertEqual(c, c1)

#--disjunction()
  def test_disjunctionShape(self):
    """[1;25] -> Circle | [26;50] -> Square | [51;75] -> Triangle | [76;100] -> Line"""
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    c2 = Circle((10,10),True,20,90,"#AAAAAA")
    c = br.disjunctionShape(c1,c2,1)
    self.assertEqual(c, c1,str(c) + " != " + str(c1))
    c3 = Square((10,10),True,20,90,"#AAAAAA")
    c = br.disjunctionShape(c1,c2,2)
    self.assertEqual(c, c1)

#--exclusiveDisjunction()
  def test_exclusiveDisjunctionShape(self):
    """[1;25] -> Circle | [26;50] -> Square | [51;75] -> Triangle | [76;100] -> Line"""
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    c2 = Circle((10,10),True,20,90,"#AAAAAA")
    c = br.exclusiveDisjunctionShape(c1,c2,4)
    self.assertEqual(c, Nulle())
    c3 = Square((10,10),True,20,90,"#AAAAAA")
    c = br.exclusiveDisjunctionShape(c1,c3,1)
    self.assertEqual(c, c1)



    