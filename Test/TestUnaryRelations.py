import unittest
from  Generator.Figures.Circle import Circle
from  Generator.Figures.Triangle import Triangle
import Generator.Relations.BinaryRelation as br
import Generator.Relations.UnaryRelation as ur

class TestUnaryRelations(unittest.TestCase):
 
  def test_moveHorizontally(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    (c2,c3)  = ur.moveHorizontally(c1,5)
    self.assertEqual((27,10), c2.getPosition()) # 10 + (50 - 10 - 10//2)//2
    self.assertEqual((45,10), c3.getPosition()) # 10 + (50 - 10 - 10//2)

  def test_moveVertically(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    (c2,c3) = ur.moveVertically(c1,5)
    self.assertEqual((10,27), c2.getPosition()) # 10 + (50 - 10 - 10//2)//2
    self.assertEqual(((10),45), c3.getPosition()) # 10 + (50 - 10 - 10//2)

  def test_crossCase(self):
    c1 = Circle((10,10),True,10,15,"#AAAAAA")
    (c2,c3) = ur.crossCase(c1,5)
    self.assertEqual((27, 27), c2.getPosition()) # 10 + (50 - 10 - 10//2)//2
    self.assertEqual((45, 45), c3.getPosition()) # 10 + (50 - 10 - 10//2)

  def test_rotation(self) :
    c1 = Triangle((10,10),True,10,15,"#AAAAAA")
    (c2,c3) = ur.rotation(c1,20)
    self.assertEqual(25, c2.getOrientation())
    self.assertEqual(35, c3.getOrientation())

  def test_modifySize(self) :
    c1 = Triangle((10,10),True,10,15,"#AAAAAA")
    (c2,c3) = ur.modifySize(c1,5)
    self.assertEqual(25, c2.getSize()) # 10 + (50 - 10 - 10)//2
    self.assertEqual(40, c3.getSize()) # 10 + (50 - 10 - 10)