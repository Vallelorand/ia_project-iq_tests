import unittest
from  Generator.Figures.Square import Square
from  Generator.Figures.Triangle import Triangle
from  Generator.Figures.Circle import Circle
from  Generator.Figures.Nulle import Nulle
from  Generator.RPM.Case import Case

class TestCases(unittest.TestCase):
  
  def test_creationCase(self):
    c1 = Case()
    self.assertEqual(type(c1), Case)
    self.assertEqual(c1.nombreFigure(), 0)
    self.assertEqual(c1,Case())

  def test_addFigure(self):
    c1 = Case()
    c1.addFigure(Nulle())
    self.assertEqual(c1.nombreFigure(), 1)
    c1.addFigure(Triangle((0,0),False,20,90,"#AAAAAA"))
    self.assertEqual(c1.nombreFigure(), 2)
    self.assertEqual(c1.getFigure(0), Nulle())

  def test_nombreFigure(self):
    c1 = Case()
    for i in range(5000) :
      c1.addFigure(Nulle())
    self.assertEqual(c1.nombreFigure(), 5000)
    self.assertEqual(c1.getFigure(4999), Nulle())

  def test_equalsCase(self):
    c1 = Case()
    c1.addFigure(Nulle())
    c2 = Case()
    c2.addFigure(Nulle())
    self.assertEqual(c1, c2)

  def test_replaceFigure(self) :
    c1 = Case()
    c1.addFigure(Triangle((0,0),False,20,90,"#AAAAAA"))
    f1= Circle((10,10),True,10,90,"#AAAAAA")
    c1.replace(0,f1)
    self.assertEqual(c1.getFigure(0), f1)

  def test_replaceFigureAssert(self) :
    c1 = Case()
    c1.addFigure(Triangle((0,0),False,20,90,"#AAAAAA"))
    f1= Circle((10,10),True,10,90,"#AAAAAA")
    self.assertRaises(AssertionError,c1.replace,1,f1)

  def test_getFigure(self):
    c1 = Case()
    c1.addFigure(Nulle())
    c1.addFigure(Square((10,10),True,10,0,"#AAAAAA"))
    self.assertEqual(c1.getFigure(0), Nulle())
    self.assertEqual(c1.getFigure(1), Square((10,10),True,10,0,"#AAAAAA"))

  def test_getFigureAssert(self) :
    c1 = Case()
    c1.addFigure(Triangle((0,0),False,20,90,"#AAAAAA"))
    self.assertRaises(AssertionError,c1.getFigure,5)